1. Install jsonnet-bundler: `go get github.com/jsonnet-bundler/jsonnet-bundler/cmd/jb`
2. Install dependencies: `jb install`
3. Install gojsontoyaml: `go get github.com/brancz/gojsontoyaml`
4. Generate manifests: `./generate`
