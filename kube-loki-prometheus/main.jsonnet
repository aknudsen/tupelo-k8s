local lokiProm = import 'kube-loki-prometheus/kube-loki-prometheus.libsonnet';

function(smtpToken='', smtpUsername='')
  lokiProm.manifests(lokiStorageSize='100Gi', lokiNamespace='logging',
    promNamespace='monitoring', extraConfig={
    grafana+:: {
      config+: {
        sections+: {
          users: {
            auto_assign_org: true,
            auto_assign_org_role: 'Admin',
          },
          'auth.proxy': {
            enabled: true,
            header_name: 'X-Forwarded-User',
            header_property: 'username',
            auto_sign_up: true,
            headers: 'Email:X-Forwarded-Email',
          },
          'auth.basic': {
            enabled: false,
          },
          'auth': {
            disable_signout_menu: true,
          },
        },
      },
    },
    alertmanager+:: {
      config+: {
        global+: {
          smtp_from: 'alertmanager@experimental.berlin',
          smtp_smarthost: 'smtp.mandrillapp.com:587',
          smtp_auth_username: smtpUsername,
          smtp_auth_password: smtpToken,
        },
        route: {
          receiver: 'default',
          group_by: [
            'job',
          ],
          routes: [
            {
              receiver: 'null',
              match: {
                alertname: 'Watchdog',
              },
            },
          ],
          group_wait: '30s',
          group_interval: '5m',
          repeat_interval: '12h',
        },
        receivers+: [
          {
            name: 'default',
            email_configs: [
              {
                to: 'contact@experimental.berlin',
              },
            ],
          },
        ],
      },
    }
  })
