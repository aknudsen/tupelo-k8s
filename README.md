# Tupelo Kubernetes Cluster
Definitions for [Tupelo](https://docs.quorumcontrol.com/docs/litepaper.html) Kubernetes cluster.

The cluster manifests are generated from templates in the directory *templates/*. Instantiate
them by executing the script *generate-manifests*.

To deploy the cluster, apply manifests generated in *build/production*:

```
./generate-manifests
./kubectl-production apply -f build/production
```

## Cluster Topology
The cluster consists of three signer nodes and three bootstrap nodes. The latter group of nodes
is necessary for peers to bootstrap and become connected within the network. Each bootstrap/signer
node is assigned a load balancing Kubernetes service that exposes the corresponding
StatefulSet pod (via the `statefulset.kubernetes.io/pod-name` selector), which ensures in effect
that it gets a public IP address so external clients can connect.

### Bootstrap Nodes
Each of the bootstrap nodes is assigned a dedicated ECDSA key pair.

This is solved by using a StatefulSet to create each bootstrap node, as each pod in a StatefulSet
has a sticky identity. This identity is used to get the corresponding ECDSA private key
from the filesystem (the containing Kubernetes secret is mounted as a directory,
*/etc/tupelo/private-keys/bootstrap-<index>.json*), and nodes that
want to connect to the bootstrap nodes get the peer IDs also from the filesystem (made available
by mounting a Kubernetes ConfigMap).

Additionally, each bootstrap node gets its own public IP from its corresponding public key file
(*/etc/tupelo/public-keys/bootstrap-<index>.json*) and configures it towards libp2p. This allows
the nodes to be connected to from outside the cluster. Furthermore, each bootstrap node
finds other bootstrap nodes by enumerating the aforementioned public key files and connects
to them.

### Signer Nodes
Each of the signer nodes is assigned a dedicated ECDSA key pair.

This is solved by using a StatefulSet to create each signer node, as each pod in a StatefulSet
has a sticky identity. This identity is used to get the corresponding ECDSA private key
from the filesystem (the containing Kubernetes secret is mounted as a directory). The signer
public keys are also mounted in the filesystem from a Kubernetes ConfigMap.

Additionally, each signer node gets its own public IP from its corresponding public key file
(*/etc/tupelo/public-keys/signer-<index>.json*) and configures it towards libp2p. This allows the
nodes to be connected to from outside the cluster.

The signer nodes also find the bootstrap nodes by enumerating their public key files
(*/etc/tupelo/public-keys/bootstrap-<index>.json*), and connect to them in order to become
part of the p2p network and discoverable by clients.

## Logging and Monitoring
For logging and monitoring, we use a stack mainly consisting of Prometheus, Alertmanager, Loki
and Grafana. We'll explain our use of these systems more in depth in the following
sections, but first of all we want to instruct you in how to install the stack, as it's done
all together.

For deployment of this stack, we use the
[kube-loki-prometheus](https://gitlab.com/aknudsen/kube-loki-prometheus) Jsonnet library.
To deploy it, please follow this procedure:

```
cd kube-loki-prometheus
jb install
./generate
cd ..
./kubectl-production apply -f kube-loki-prometheus/build
```

Sometimes the last step above will fail, because custom resources deployed by Kube-Prometheus
aren't ready to handle resource definitions directed at them (i.e., a timing issue).
If that happens, wait a bit before trying to to apply the manifests again.

### Logging

We use the [Loki](https://grafana.com/loki) logging backend in the cluster for aggregating
logs in a searchable manner. Loki gets connected to Grafana as a data source, so you can
browse logs in Grafana by going to its Explore section and choosing Loki as the data source.

Read further down for how to access Grafana.

### Monitoring
The Tupelo cluster is monitored with [Prometheus](https://prometheus.io/), combined with
Alertmanager for sending alerts, and we use [Grafana](https://grafana.com/) to visualize the
metrics gathered by the former. The Kube-Prometheus stack, which installs Prometheus, Alertmanager
and Grafana, also installs monitoring/alerting rules and Grafana dashboards for us.

### Grafana

To connect to Grafana, and visualize the monitoring metrics from Prometheus or browsing Loki logs,
visit https://grafana.experimental.berlin and log in with an authorized address.

Alternatively, you can use `kubectl` to locally proxy a connection to Grafana like so:

```
./kubectl-production -nmonitoring port-forward svc/grafana 3000
```

Then you can visit Grafana in your browser by opening http://localhost:3000.
